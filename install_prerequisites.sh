#!/bin/bash
KIND_BINARY_VERSION="0.24.0"
CPK_BINARY_VERSION="0.3.0"
CILIUM_CLI_VERSION="v0.16.18"
PATH=$PATH:~/.kind

if ! command -v cloud-provider-kind &> /dev/null
then
    echo "installing cloud-provider-kind binary in ~/.kind"
    curl -sL --remote-name-all\
    "https://github.com/kubernetes-sigs/cloud-provider-kind/releases/download/v${CPK_BINARY_VERSION}/cloud-provider-kind_${CPK_BINARY_VERSION}_linux_amd64.tar.gz"
    mkdir -p ~/.kind
    tar xzvfC cloud-provider-kind_${CPK_BINARY_VERSION}_linux_amd64.tar.gz ~/.kind
    rm cloud-provider-kind_${CPK_BINARY_VERSION}_linux_amd64.tar.gz
fi

if ! command -v kind &> /dev/null
then
    echo "installing kind binary in ~/.kind"
    curl -sLo ./kind \
    "https://kind.sigs.k8s.io/dl/v${KIND_BINARY_VERSION}/kind-$(uname)-amd64"
    chmod +x ./kind
    mkdir -p ~/.kind
    mv ./kind  ~/.kind/kind
fi

if ! command -v cilium &> /dev/null
then
    echo "installing cilium binary in ~/.kind"
    CLI_ARCH=amd64
    curl -sL --fail --remote-name-all https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
    sha256sum --check cilium-linux-${CLI_ARCH}.tar.gz.sha256sum
    tar xzvfC cilium-linux-${CLI_ARCH}.tar.gz ~/.kind
    rm cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
fi
