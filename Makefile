.PHONY: help
help:  ## 🤔 Show help messages for make targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}'

CLUSTER_NAME=`kind get clusters -q`
CONTROLLER_PID=`pidof "cloud-provider-kind"`

cluster: $(eval SHELL:=/bin/bash) ## Creates the kind k8s cluster
	@if [ "$(CLUSTER_NAME)" != "local-dev-cluster" ]; then kind create cluster --config local-dev-cluster.yaml; fi
	-@ helm list -n kube-system -o json| jq -er '.[] | select(.name == "cilium")|.name' &>/dev/null;\
	if [ $$? -ne 0 ]; \
    	then \
        echo "Installing Cilium"; \
		cilium install;\
    fi

cilium_test: ## Use cilium cli to test connectivity
	cilium status --wait
	cilium connectivity test
	kubectl delete ns -l app.kubernetes.io/name=cilium-cli
	
soft: lb ## Deploys default apps in the cluster
	@echo "installing default apps:"
	@find resources  -mindepth 1 -type d
	kustomize build | kubectl apply --dry-run=client -f - &&\
	kustomize build | kubectl apply -f -

soft_clean:  ## Deletes apps but keep the cluster
	kustomize build | kubectl delete  --ignore-not-found=true -f -

clean: clean_lb ## Deletes the kind cluster
	kind delete cluster -n $(CLUSTER_NAME)

full_clean: clean ## Delete kind cluster and removes prerequisites
	rm -rf ~/.kind

lb: cluster ## Instanciate cloud controller provider to handle external load balancers
	@if [ -z "${CONTROLLER_PID}" ]; then \
		nohup ~/.kind/cloud-provider-kind > /tmp/lb.out & \
	else \
	echo "cloud-provider-kind is already running with PID: ${CONTROLLER_PID}"; \
	fi

clean_lb: ## Delete cloud controller provider
	@if [ ! -z "${CONTROLLER_PID}" ]; then \
		kill $(CONTROLLER_PID); \
	fi

install: ## Installs kind, cloud-provider-kind and its user systemd unit
	 @echo "Installing..."
	 bash install_prerequisites.sh
	 @echo "Please add ~/.kind to PATH" 

status: ## Display current status
	@kind get clusters
	@kubectl cluster-info
	@kubectl get nodes
	@echo ===========
	@echo "lists of default apps:"
	@find resources  -mindepth 1 -type d

##
#@echo 'Increasing inotify watches, please enter your sudo password'
#	sudo sysctl fs.inotify.max_user_watches=524288
#	sudo sysctl fs.inotify.max_user_instances=512