<p align="center"><img alt="kind" src="./logo/logo.png" width="300x" /></p>

### View The Documentation

This tool helps you run a local kubernetes cluster using `kind`. It leverages the `cloud-provider-kind` to offer external load balancer capability and also provides a list of default apps. (See next section)

## Installation and usage

To use this project , you will need to install, jq , docker >= 19 , kind >= 0.24.0 and kubectl >= 1.31.0, helm >= 3.15.4


Create cluster:

```console
make cluster
```

Create cluster and deploy default apps:

```console
make soft
```

Delete cluster:

```console
make clean
```

## Default apps

By default this tool deploys:
* Cilium as CNI
* ArgoCD
* Traefik as ingress controller

## Advanced usage

Please use

```console
make help
```

To see advanced options

### Cluster configuration

You can modify `local-dev-cluster.yaml` to modify node number and role.

